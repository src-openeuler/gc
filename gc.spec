Name:    gc
Version: 8.2.8
Release: 1
Summary: A garbage collector for C and C++
License: BSD-3-Clause
Url:     http://www.hboehm.info/gc/
Source0: https://github.com/ivmai/bdwgc/releases/download/v%{version}/gc-%{version}.tar.gz
Patch1:  gc-8.0.6-sw.patch
Patch2:  0001-add-loongarch-architecture.patch
Patch3:  gc-8.2.6-make-atomic_ops_private.patch

BuildRequires: gcc-c++
BuildRequires: autoconf automake libtool
BuildRequires: pkgconfig(atomic_ops)

%description
The Boehm-Demers-Weiser conservative garbage collector can be
used as a garbage collecting replacement for C malloc or C++ new.

%package devel
Summary: Libraries and header files for %{name} development
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
%{summary}.

%prep
%autosetup -n %{name}-%{version} -p1


%build
# refresh auto*/libtool to purge rpaths
rm -f libtool libtool.m4
autoreconf -i -f

# See https://bugzilla.redhat.com/show_bug.cgi?id=689877
CPPFLAGS="-DUSE_GET_STACKBASE_FOR_MAIN"; export CPPFLAGS

%configure \
  --disable-static \
  --disable-docs \
  --enable-cplusplus \
  --enable-large-config \
  --enable-threads=posix \
  --with-libatomic-ops=yes

%{make_build}


%install
%{make_install}
%delete_la
install -p -D -m644 doc/gc.man  %{buildroot}%{_mandir}/man3/gc.3

## Delete unpackaged files
rm -rfv %{buildroot}%{_datadir}/gc/


%check
%make_build check

%files
%doc AUTHORS ChangeLog README.md
%{_libdir}/libcord.so.1*
%{_libdir}/libgc.so.1*
%{_libdir}/libgccpp.so.1*
%{_libdir}/libgctba.so.1*

%files devel
%doc doc/README.environment doc/README.linux
%{_includedir}/gc.h
%{_includedir}/gc_cpp.h
%{_includedir}/gc
%{_libdir}/libcord.so
%{_libdir}/libgc.so
%{_libdir}/libgccpp.so
%{_libdir}/libgctba.so
%{_libdir}/pkgconfig/bdw-gc.pc
%{_mandir}/man3/gc.3*

%changelog
* Mon Sep 09 2024 Funda Wang <fundawang@yeah.net> - 8.2.8-1
- update to 8.2.8

* Fri Aug 23 2024 Funda Wang <fundawang@yeah.net> - 8.2.6-3
- move atomic_ops libs into private so that it won't confuse downstream packages

* Thu Aug 15 2024 Funda Wang <fundawang@yeah.net> - 8.2.6-2
- move libgctba.so into devel package
- build against system atomic_ops

* Wed Jul 24 2024 dillon chen <dillon.chen@gmail.com> - 8.2.6-1
- Updade to 8.2.6

* Sun Oct 8 2023 huyubiao <huyubiao@huawei.com> - 8.2.4-1
- Update gc to 8.2.4

* Thu Feb 2 2023 huyubiao <huyubiao@huawei.com> - 8.2.2-1
- Update gc to 8.2.2

* Mon Nov 21 2022 doupengda <doupengda@loongson.cn> - 8.0.6-4
- add loongarch64 support

* Mon Nov 14 2022 wuzx<wuzx1226@qq.com> - 8.0.6-3
- Type:feature
- CVE:NA
- SUG:NA
- DESC:Add sw64 architecture

* Tue Oct 18 2022 Liu Zixian <liuzixian4@huawei.com> - 8.0.6-2
- Type:cleancode
- ID:NA
- SUG:NA
- DESC:Remove unused BuildRequires

* Thu Dec 2 2021 wangjie <wangjie375@huawei.com> - 8.0.6-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Update gc to 8.0.6

* Thu Jul 22 2021 wangchen<wangchen137@huawei.com> - 8.0.4-4
- Deleta unnecessary gdb from BuildRequires

* Fri Jun 4 2021 panxiaohe<panxiaohe@huawei.com> - 8.0.4-3
- add gcc-c++ to BuildRequires to use c++ command

* Sat Mar 14 2020 shenyangyang<shenyangyang4@huawei.com> - 8.0.4-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add build requires of gdb

* Tue Nov 5 2019 shenyangyang<shenyangyang4@huawei.com> - 8.0.4-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update gc to 8.0.4 to solve build problem of guile

* Wed Sep 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 7.6.12-2
- Modify license

* Thu Aug 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 7.6.12-1
- Package init
